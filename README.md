# Instant Messaging

This project is an instant messaging application, developed for CS314 - Elem Software Engr. It is written primarily in JavaScript using the MERN stack. It meets all feature requirements outlined in the term project description and utilizes a MongoDB database for storing and retrieving users, groups, and individual chat messages.

## Project Structure and Implementation

The project is structured into two main parts: the **frontend** and the **backend**.

### Frontend
The frontend of our application, developed using ReactJS, is the driving force behind the user interface and user experience. It operates independently from the backend, hosted separately on localhost:3001. The entirety of the frontend code resides within the client folder. Our frontend is streamlined and user-friendly, consisting of two main pages. The default page, accessible via /, provides users with the ability to either register a new account or log in to an existing one. The second page, /chat, encompasses all other functionality required for the project.

### Backend
The backend was developed using NodeJS, Express, and Mongoose for MongoDB, handles data processing, server-side logic, and database operations.

A backend request has the following form:
1. Specify the domain. In our case it was `localhost:3000`
2. A prefix notes which part of the DB we are accessing, EG: `.../users`. See `app.js` for the full prefix list.
3. A command list is available depending on the selected prefix (see **2.**). The commands available are listed in the corresponding `/routes/*.js` file. EG: for a user command, this would be in `/routes/users.js`.
    * The implementations for these functions are in the `/controllers` directory.
4. If a command takes a **request** body, EG: editing a user with `PATCH localhost:3000/users/:id`, the body can only contain information that fits the form of the database fields for that object.
    * The fields available for each database section are located in `/models`.

**Example:** If we wanted to edit a user's name, the total command would look like:
```
PATCH localhost:3000/users/665be4c2da6889b10e5f8539

{
    "Name": "Amiel Iliesi"
}
```

Finally, the backend implementation ends with the files `app.js` and `/bin/www` which respectively contain the app API, and the backend starting functionality.

## Participants

### Group 18
* Amiel Iliesi
* Tanner Blake

## Technology and Responsibilities

### Frontend
Tanner Blake:
* ReactJS

### Backend
Amiel Iliesi:
* NodeJS
* Express
* Mongoose

### Database
Amiel Iliesi:
* MongoDB

### Source Control
Tanner Blake, Amiel Iliesi:
* Git
* GitLab

## Testing

We did not utilize automated testing within our application. Instead, the backend was tested using Postman to send HTTP requests and manually verify their correctness. The frontend was tested by developing code that logged system errors and manually verifying that all features performed the intended behavior within the database.

## Deployment

Our website is not currently deployed.

## Repository Access

Our repository is public and can be accessed at https://gitlab.cecs.pdx.edu/ailiesi/cs333-001-final_project.


