const Group = require('../models/groupModel');
const mongoose = require('mongoose');

//GET all groups
const getGroups = async (req, res) => {
    const groups = await Group.find({}).sort({updatedAt: -1}); //by most recently updated

    res.status(200).json(groups);
}

//GET a single group by ID
//return a single group
const getGroup = async (req, res) => {
    const { id } = req.params;

    if (!mongoose.Types.ObjectId.isValid(id))
        return res.status(400).json({error: 'ID is invalid'});
        
    const group = await Group.findById(id);

    if (!group)
        return res.status(404).json({error: 'No such group ID'});
    
    res.status(200).json(group);
}

//CREATE a new group
const createGroup = async (req, res) => {
    const {Name, Groups} = req.body;

    try {
        const group = await Group.create({Name, Groups});
        res.status(200).json(group);
    } catch (error) {
        res.status(400).json({error: error.message});
    }
}

//DELETE a group
const deleteGroup = async (req, res) => {
    const { id } = req.params;

    if (!mongoose.Types.ObjectId.isValid(id))
        return res.status(400).json({error: 'ID is invalid'});

    const group = await Group.findOneAndDelete({_id: id});

    if (!group)
        return res.status(404).json({error: 'No such group ID'});
    
    res.status(200).json(group);
}

//UPDATE a group
const updateGroup = async (req, res) => {
    const { id } = req.params;

    if (!mongoose.Types.ObjectId.isValid(id))
        return res.status(400).json({error: 'ID is invalid'});

    const group = await Group.findOneAndUpdate({_id: id}, {...req.body});

    if (!group)
        return res.status(404).json({error: 'No such group ID'});
    
    res.status(200).json(group);
}

module.exports = {
    getGroups,
    getGroup,
    createGroup,
    deleteGroup,
    updateGroup
}