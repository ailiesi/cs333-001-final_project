const User = require('../models/userModel');
const Group = require('../models/groupModel'); //need so users can join/leave groups
const mongoose = require('mongoose');

//return all users
const getUsers = async (req, res) => {
    const users = await User.find({}).sort({updatedAt: -1});

    res.status(200).json(users);
}

//return a single user
const getUser = async (req, res) => {
    const { id } = req.params;

    if (!mongoose.Types.ObjectId.isValid(id))
        return res.status(400).json({error: 'ID is invalid'});
        
    const user = await User.findById(id);

    if (!user)
        return res.status(404).json({error: 'No such user ID'});
    
    res.status(200).json(user);
}

//create new user
const createUser = async (req, res) => {
    const {Name, PassHash, Friends, Groups} = req.body;

    try {
        const user = await User.create({Name, PassHash, Friends, Groups});
        res.status(200).json(user);
    } catch (error) {
        res.status(400).json({error: error.message});
    }
}

//delete a user
const deleteUser = async (req, res) => {
    const { id } = req.params;

    if (!mongoose.Types.ObjectId.isValid(id))
        return res.status(400).json({error: 'ID is invalid'});

    const user = await User.findOneAndDelete({_id: id});

    if (!user)
        return res.status(404).json({error: 'No such user ID'});
    
    res.status(200).json(user);
}

//update a user
const updateUser = async (req, res) => {
    const { id } = req.params;

    if (!mongoose.Types.ObjectId.isValid(id))
        return res.status(400).json({error: 'ID is invalid'});

    const user = await User.findOneAndUpdate({_id: id}, {...req.body});

    if (!user)
        return res.status(404).json({error: 'No such user ID'});
    
    res.status(200).json(user);
}

const addFriend = async (req, res) => {
    const ID = req.params.userID;
    const friendID = req.params.friendID;

    //1. assert that IDs are valid IDs
    if (!mongoose.Types.ObjectId.isValid(ID))
        return res.status(400).json({error: 'ID is invalid'});

    if (!mongoose.Types.ObjectId.isValid(friendID))
        return res.status(400).json({error: 'Friend ID is invalid'});

    //2. assert user and friend are IDs in the DB
    const friend = await User.findOne({_id: friendID});

    if (!friend)
        return res.status(404).json({error: 'No such friend ID'});

    const user = await User.findOne({_id: ID});

    if (!user)
        return res.status(404).json({error: 'No such user ID'});

    //only add friend to user IF it's not already in the list
    if (!user.Friends.includes(friendID)) { //no duplicates
        await User.findOneAndUpdate({_id: ID}, {"$push": {Friends: friendID}}, {new: true});
        await User.findOneAndUpdate({_id: friendID}, {"$push": {Friends: ID}}, {new: true}); //bidirectional add
    }
    else
        return res.status(400).json({error: "User was already in friends list"});
    
    res.status(200).json(user);
}

//XXX YUCK! lots of repetition with addFriend, joinGroup, etc. No time to figure enough JS to make
//    it any nicer though. Might be worth looking at in any future refactor.
const removeFriend = async (req, res) => {
    const ID = req.params.userID;
    const friendID = req.params.friendID;

    //1. assert that IDs are valid IDs
    if (!mongoose.Types.ObjectId.isValid(ID))
        return res.status(400).json({error: 'ID is invalid'});

    if (!mongoose.Types.ObjectId.isValid(friendID))
        return res.status(400).json({error: 'Friend ID is invalid'});

    //2. assert user and friend are IDs in the DB
    const friend = await User.findOne({_id: friendID});

    if (!friend)
        return res.status(404).json({error: 'No such friend ID'});

    const user = await User.findOne({_id: ID});

    if (!user)
        return res.status(404).json({error: 'No such user ID'});

    //only remove friend to user IF is in the list
    if (user.Friends.includes(friendID)) { //no duplicates
        await User.findOneAndUpdate({_id: ID}, {"$pull": {Friends: friendID}}, {new: true});
        await User.findOneAndUpdate({_id: friendID}, {"$pull": {Friends: ID}}, {new: true}); //bidirectional remove
    }
    else
        return res.status(404).json({error: "User was not in the friends list"});
    
    res.status(200).json(user);
}

//FIXME: this is UNTESTED, we need a group example to add *into*
const joinGroup = async (req, res) => {
    const ID = req.params.userID;
    const groupID = req.params.groupID;

    //1. assert that IDs are valid IDs
    if (!mongoose.Types.ObjectId.isValid(ID))
        return res.status(400).json({error: 'ID is invalid'});

    if (!mongoose.Types.ObjectId.isValid(groupID))
        return res.status(400).json({error: 'group ID is invalid'});

    //2. assert user and group are IDs in the DB
    const group = await Group.findOne({_id: groupID});

    if (!group)
        return res.status(404).json({error: 'No such group ID'});

    const user = await User.findOne({_id: ID});

    if (!user)
        return res.status(404).json({error: 'No such user ID'});

    //only add friend to user IF it's not already in the list
    if (!user.Groups.includes(groupID)) { //no duplicates
        await User.findOneAndUpdate({_id: ID}, {"$push": {Groups: groupID}}, {new: true});
        await Group.findOneAndUpdate({_id: groupID}, {"$push": {Users: ID}}, {new: true}); //bidirectional add
    }
    else
        return res.status(400).json({error: "User was already in the Group"});
    
    res.status(200).json(user);
}

const removeGroup = async (req, res) => {
    const ID = req.params.userID;
    const groupID = req.params.groupID;

    //1. assert that IDs are valid IDs
    if (!mongoose.Types.ObjectId.isValid(ID))
        return res.status(400).json({error: 'ID is invalid'});

    if (!mongoose.Types.ObjectId.isValid(groupID))
        return res.status(400).json({error: 'group ID is invalid'});

    //2. assert user and group are IDs in the DB
    const group = await Group.findOne({_id: groupID});

    if (!group)
        return res.status(404).json({error: 'No such group ID'});

    const user = await User.findOne({_id: ID});

    if (!user)
        return res.status(404).json({error: 'No such user ID'});

    //only remove group from user IF it's in the list
    if (user.Groups.includes(groupID)) { //no duplicates
        await User.findOneAndUpdate({_id: ID}, {"$pull": {Groups: groupID}}, {new: true});
        await Group.findOneAndUpdate({_id: groupID}, {"$pull": {Users: ID}}, {new: true}); //bidirectional remove
    }
    else
        return res.status(404).json({error: "User was not in the Group"});
    
    res.status(200).json(user);
}

module.exports = {
    getUsers,
    getUser,
    createUser,
    deleteUser,
    updateUser,
    addFriend,
    removeFriend,
    joinGroup,
	removeGroup
}