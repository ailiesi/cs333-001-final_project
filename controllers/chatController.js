const Chat = require('../models/chatModel');
const Group = require('../models/groupModel'); //can't send a chat *to* a non-existent group
const User = require('../models/userModel'); //can't send a chat *from* a non-existent user
const mongoose = require('mongoose');

//return all chats
const getAllChats = async (req, res) => {
    const chats = await Chat.find({}).sort({createdAt: -1}); //return chats by most recently created.

    res.status(200).json(chats);
}

//return a single group's chats
const getChats = async (req, res) => {
    const { id } = req.params;

    if (!mongoose.Types.ObjectId.isValid(id))
        return res.status(400).json({error: 'ID is invalid'});
        
    const group = await Group.findById(id);

    if (!group)
        return res.status(404).json({error: 'No such group ID'});

    const chats = await Chat.find({GroupID: id}).sort({createdAt: -1});
    res.status(200).json(chats);
}

//return an individual message
const getChat = async (req, res) => {
    const { id } = req.params;

    if (!mongoose.Types.ObjectId.isValid(id))
        return res.status(400).json({error: 'ID is invalid'});
        
    const chat = await Chat.findById(id);

    if (!chat)
        return res.status(404).json({error: 'No such message ID'});
    
    res.status(200).json(chat);
}

//create new chat
const sendMessage = async (req, res) => {
    const {Contents, Sender, GroupID} = req.body;

    try {
        if (!mongoose.Types.ObjectId.isValid(Sender))
            return res.status(400).json({error: 'Sender ID is invalid'});

        if (!mongoose.Types.ObjectId.isValid(GroupID))
            return res.status(400).json({error: 'Group ID is invalid'});

        const group = await Group.findById(GroupID);

        if (!group)
            return res.status(404).json({error: 'No such group ID'});

        const sender = await User.findById(Sender);

        if (!sender)
            return res.status(404).json({error: 'No such user ID'});

        const chat = await Chat.create({Contents, Sender, GroupID});
        res.status(200).json(chat);
    } catch (error) {
        res.status(400).json({error: error.message});
    }
}

//delete a chat
const deleteMessage = async (req, res) => {
    const { id } = req.params;

    if (!mongoose.Types.ObjectId.isValid(id))
        return res.status(400).json({error: 'ID is invalid'});

    const chat = await Chat.findOneAndDelete({_id: id});

    if (!chat)
        return res.status(404).json({error: 'No such message ID'});
    
    res.status(200).json(chat);
}

//clear a group's chat
const deleteMessages = async (req, res) => {
    const { id } = req.params;

    if (!mongoose.Types.ObjectId.isValid(id))
        return res.status(400).json({error: 'ID is invalid'});

    const chat = await Chat.deleteMany({GroupID: id});

    if (!chat)
        return res.status(404).json({error: 'No such group ID'});
    
    res.status(200).json(chat);
}

//edit a message
const editMessage = async (req, res) => {
    const { id } = req.params;

    if (!mongoose.Types.ObjectId.isValid(id))
        return res.status(400).json({error: 'ID is invalid'});

    const chat = await Chat.findOneAndUpdate({_id: id}, {...req.body});

    if (!chat)
        return res.status(404).json({error: 'No such message ID'});
    
    res.status(200).json(chat);
}

module.exports = {
    getAllChats,
    getChats,
    getChat,
    sendMessage,
    deleteMessage,
    deleteMessages,
    editMessage
}