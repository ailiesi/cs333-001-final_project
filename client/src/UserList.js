import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { useLocation } from 'react-router-dom';

function UserList() {
    const location = useLocation();
    const currentUserId = location.state?.userId;
	const username = location.state?.username;
    const [users, setUsers] = useState([]);
    const [groups, setGroups] = useState([]);
    const [contextMenu, setContextMenu] = useState({ visible: false, x: 0, y: 0, userId: null });

    useEffect(() => {
        // Fetch all users when the component mounts
        const fetchUsers = async () => {
            try {
                const response = await axios.get('http://localhost:3000/users');
                setUsers(response.data);
            } catch (error) {
                console.error(`Error fetching users: ${error}`);
            }
        };

        // Fetch the current user's groups when the component mounts
        const fetchUserGroups = async () => {
            try {
                const response = await axios.get(`http://localhost:3000/users/${currentUserId}`);
                const userGroups = response.data.Groups;
                const groupDetails = await Promise.all(userGroups.map(groupId => 
                    axios.get(`http://localhost:3000/groups/${groupId}`)
                ));
                setGroups(groupDetails.map(response => response.data));
            } catch (error) {
                console.error(`Error fetching user's groups: ${error}`);
            }
        };

        fetchUsers();
        fetchUserGroups();
    }, [currentUserId]);

    const handleContextMenu = (event, userId) => {
        event.preventDefault();
        setContextMenu({ visible: true, x: event.clientX, y: event.clientY, userId });
    };

    const hideContextMenu = () => {
        setContextMenu({ visible: false, x: 0, y: 0, userId: null });
    };

    const handleAddToGroup = async (groupId) => {
		try {
			await axios.patch(`http://localhost:3000/users/${contextMenu.userId}/joinGroup/${groupId}`);
			hideContextMenu();
		} catch (error) {
			console.error(`Error adding user to group: ${error}`);
		}
	};

    return (
        <div onClick={hideContextMenu}>
			<p>Welcome, {username}!</p>
            <h2>User List:</h2>
            {users.map((user) => (
                <div key={user._id} onContextMenu={(event) => handleContextMenu(event, user._id)}>
                    {user.Name}
                </div>
            ))}
            {contextMenu.visible && (
                <div
                    style={{
                        position: 'absolute',
                        top: contextMenu.y,
                        left: contextMenu.x,
                        backgroundColor: 'white',
                        border: '1px solid black',
                        padding: '10px',
                        zIndex: 1000
                    }}
                >
                    {groups.map((group) => (
                        <button key={group._id} onClick={() => handleAddToGroup(group._id)}>
                            Add to {group.Name}
                        </button>
                    ))}
                </div>
            )}
        </div>
    );
}

export default UserList;