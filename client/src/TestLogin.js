import React, { useState } from 'react';
import axios from 'axios';
import bcrypt from 'bcryptjs';
import './TestLogin.css';
import { useNavigate } from 'react-router-dom';

function TestLogin() {
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const navigate = useNavigate();

    const handleSubmit = async (event) => {
        event.preventDefault();
        console.log(`Username: ${username}`);
        
        // Fetch all users
        try {
            const users = await axios.get('http://localhost:3000/users');
            let user = users.data.find(user => user.Name === username);
            
            if (user && bcrypt.compareSync(password, user.PassHash)) {
                console.log('User already exists, logging in...');
            } else {
                // Hash and salt the password
                const salt = bcrypt.genSaltSync(10);
                const hashedPassword = bcrypt.hashSync(password, salt);

                // Create a new user
                try {
                    const response = await axios.post('http://localhost:3000/users', {
                        Name: username,
                        PassHash: hashedPassword,
                        Friends: [],
                        Groups: []
                    });
                    user = response.data;
                    console.log(user);
                } catch (error) {
                    console.error(`Error creating user: ${error}`);
                }
            }
            // Navigate to chat page
            navigate('/chat', { state: { username, userId: user._id } });
        } catch (error) {
            console.error(`Error fetching users: ${error}`);
        }
    }

    return (
        <div className="login-container">
            <form onSubmit={handleSubmit}>
                <label>
                    Username:
                    <input
                        type="text"
                        value={username}
                        onChange={(e) => setUsername(e.target.value)}
                        required
                    />
                </label>
                <label>
                    Password:
                    <input
                        type="password"
                        value={password}
                        onChange={(e) => setPassword(e.target.value)}
                        required
                    />
                </label>
                <button type="submit">Enter</button>
            </form>
        </div>
    );
}

export default TestLogin;