import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { useLocation } from 'react-router-dom';

function Chat() {
	const location = useLocation();
    const currentUserId = location.state?.userId;
    const [groups, setGroups] = useState([]);
    const [activeGroup, setActiveGroup] = useState(null);
    const [messages, setMessages] = useState([]);
	const [newMessage, setNewMessage] = useState('');
	const [users, setUsers] = useState([]);
	const [newGroupName, setNewGroupName] = useState('');

	useEffect(() => {
        // Fetch all users when the component mounts
        const fetchUsers = async () => {
            try {
                const response = await axios.get('http://localhost:3000/users');
                setUsers(response.data);
            } catch (error) {
                console.error(`Error fetching users: ${error}`);
            }
        };

        fetchUsers();
    }, []);

    useEffect(() => {
        // Fetch the current user's groups when the component mounts
        const fetchUserGroups = async () => {
            try {
                const response = await axios.get(`http://localhost:3000/users/${currentUserId}`);
                const userGroups = response.data.Groups;
                const groupDetails = await Promise.all(userGroups.map(groupId => 
                    axios.get(`http://localhost:3000/groups/${groupId}`)
                ));
                setGroups(groupDetails.map(response => response.data));
            } catch (error) {
                console.error(`Error fetching user's groups: ${error}`);
            }
        };

        fetchUserGroups();
    }, [currentUserId]);

    useEffect(() => {
        // Fetch all messages for the active group when it changes
        const fetchMessages = async () => {
			if (activeGroup) {
				try {
					const response = await axios.get(`http://localhost:3000/chats/group/${activeGroup._id}`);
					const sortedMessages = response.data.sort((a, b) => new Date(a.createdAt) - new Date(b.createdAt));
					setMessages(sortedMessages);
				} catch (error) {
					console.error(`Error fetching messages: ${error}`);
				}
			}
		};

        fetchMessages();
    }, [activeGroup]);

	const handleNewGroupNameChange = (event) => {
        setNewGroupName(event.target.value);
    };

	const handleNewGroupSubmit = async (event) => {
		event.preventDefault();
		if (newGroupName) {
			try {
				const response = await axios.post('http://localhost:3000/groups', {
					Name: newGroupName,
					Users: [currentUserId]
				});
				const newGroup = response.data;
				setGroups([...groups, newGroup]);
				setNewGroupName('');
				// Add the current user to the new group
				await axios.patch(`http://localhost:3000/users/${currentUserId}/joinGroup/${newGroup._id}`);
			} catch (error) {
				console.error(`Error creating group: ${error}`);
			}
		}
	};

    const handleGroupClick = (group) => {
        setActiveGroup(group);
    };
	
	const handleNewMessageChange = (event) => {
        setNewMessage(event.target.value);
    };

	const handleNewMessageSubmit = async (event) => {
        event.preventDefault();
        if (activeGroup) {
            try {
                const response = await axios.post('http://localhost:3000/chats', {
                    Contents: newMessage,
                    Sender: currentUserId,
                    GroupID: activeGroup._id
                });
                setMessages([...messages, response.data]);
                setNewMessage('');
            } catch (error) {
                console.error(`Error sending message: ${error}`);
            }
        }
    };

	const deleteGroup = async (groupId) => {
		try {
			// Remove the group from all users
			const usersInGroup = users.filter(user => user.Groups.includes(groupId));
			await Promise.all(usersInGroup.map(user => 
				axios.patch(`http://localhost:3000/users/${user._id}/removeGroup/${groupId}`)
			));
	
			// Delete all chats associated with the group
			await axios.delete(`http://localhost:3000/chats/group/${groupId}`);
	
			// Delete the group
			await axios.delete(`http://localhost:3000/groups/${groupId}`);
	
			// Update the local state
			setGroups(groups.filter(group => group._id !== groupId));
			if (activeGroup._id === groupId) {
				setActiveGroup(null);
			}
		} catch (error) {
			console.error(`Error deleting group: ${error}`);
		}
	};
	

    return (
        <div>
            <div 
                className="group-list" 
                style={{ 
                    overflowY: 'scroll', 
                    maxHeight: '50vh',
                    width: '20vw',
                    border: '1px solid black', 
                    padding: '10px', 
                    marginTop: '20px',
                    position: 'absolute',
                    right: '0', // Position the group list on the right side of the screen
                    textAlign: 'left'
                }}
            >
                <h2>Groups:</h2>
                {groups.map((group) => (
    				<div key={group._id} onClick={() => handleGroupClick(group)}>
        				{group.Name}
        				<button onClick={() => deleteGroup(group._id)}>Delete Group</button>
    				</div>
				))}
            </div>
            <div className="chat-box">
                {/* Display chat messages for the active group here */}
                {activeGroup && <h2>Chat for {activeGroup.Name}</h2>}
                {messages.map((message) => {
                    const sender = users.find(user => user._id === message.Sender);
					const timestamp = new Date(message.createdAt).toLocaleString();
                    return (
                        <p key={message._id}>
                            {sender ? `${sender.Name}: ` : ''}{message.Contents}
							<span style={{ fontSize: '0.8em', marginLeft: '10px' }}>{timestamp}</span>
                        </p>
                    );
                })}
    			{/* Form for submitting a new message */}
    			<form onSubmit={handleNewMessageSubmit}>
        			<input
            			type="text"
            			value={newMessage}
            			onChange={handleNewMessageChange}
            			placeholder="Type your message here"
        			/>
        			<button type="submit">Send</button>
    			</form>
			</div>
				{/* Form for creating a new group */}
            	<form onSubmit={handleNewGroupSubmit}>
                	<input
                    	type="text"
                    	value={newGroupName}
                    	onChange={handleNewGroupNameChange}
                    	placeholder="Enter new group name"
                	/>
                	<button type="submit">Create Group</button>
            	</form>
        	</div>
    );
}

export default Chat;