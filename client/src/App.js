import React from 'react';
import { BrowserRouter as Router, Route, Routes} from 'react-router-dom';
import TestLogin from './TestLogin';
import ChatPage from './ChatPage'; // Import the ChatPage component

function App() {
    return (
        <Router>
            <Routes>
                <Route path="/chat" element={<ChatPage />} /> {/* Use the ChatPage component */}
                <Route path="/" element={<TestLogin />} />
            </Routes>
        </Router>
    );
}

export default App;