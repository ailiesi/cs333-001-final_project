import React from 'react';
import UserList from './UserList';
import Chat from './Chat';

function ChatPage() {
    return (
        <div style={{ display: 'flex' }}>
            <UserList />
            <Chat />
        </div>
    );
}

export default ChatPage;