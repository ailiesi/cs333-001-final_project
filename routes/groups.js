var express = require('express');
var router = express.Router();
const { getGroups,
        getGroup,
        createGroup,
        deleteGroup, 
        updateGroup} = require('../controllers/groupController');

//GET all groups in the DB
router.get('/', getGroups);

//GET a single group from the DB
router.get('/:id', getGroup);

//CREATE a new group
router.post('/', createGroup);

/* DELETE a group */
router.delete('/:id', deleteGroup);

/* UPDATE a group */
router.patch('/:id', updateGroup);

module.exports = router;