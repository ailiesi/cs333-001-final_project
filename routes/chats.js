var express = require('express');
var router = express.Router();
const { getAllChats,
        getChats,
        getChat,
        sendMessage, 
        deleteMessage,
        deleteMessages,
        editMessage} = require('../controllers/chatController');

//GET all chats in the DB; NOTE all chats are returned by most recently created
router.get('/', getAllChats);

//GET a single group's chats from the DB
router.get('/group/:id', getChats);

//GET a single message from the DB
router.get('/:id', getChat);

//CREATE a new message
router.post('/', sendMessage);

/* DELETE group's entire chat */
router.delete('/group/:id', deleteMessages);

/* DELETE a message */
router.delete('/:id', deleteMessage);

/* EDIT a message */
router.patch('/:id', editMessage);

module.exports = router;