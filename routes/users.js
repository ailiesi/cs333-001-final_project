var express = require('express');
var router = express.Router();

const {
  createUser,
  getUsers,
  getUser,
  deleteUser,
  updateUser,
  addFriend,
  removeFriend,
  joinGroup,
  removeGroup
} = require('../controllers/userController');

/* GET all user listings */
router.get('/', getUsers);

/* GET a single user listing. */
router.get('/:id', getUser);

/* CREATE a new user */
router.post('/', createUser);

/* DELETE a user */
router.delete('/:id', deleteUser);

/* UPDATE a user */
router.patch('/:id', updateUser);

/* ADD a friend to the user */
router.patch('/:userID/addFriend/:friendID', addFriend);

/* REMOVE a friend from the user */
router.patch('/:userID/removeFriend/:friendID', removeFriend);

/* JOIN a group */
router.patch('/:userID/joinGroup/:groupID', joinGroup);

/* REMOVE a group from the user */
router.patch('/:userID/removeGroup/:groupID', removeGroup);

module.exports = router;
