const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const ChatSchema = new Schema({
    Contents: {
        type: String,
        required: true
    },
    Sender: {
        type: mongoose.Types.ObjectId,
        required: true
    },
    GroupID: {
        type: mongoose.Types.ObjectId,
        required: true
    }
}, { timestamps: true });

module.exports = mongoose.model('Chat', ChatSchema);