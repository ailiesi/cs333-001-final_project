const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const GroupSchema = new Schema({
    Name: {
        type: String,
        required: true
    },
    Users: {
        type: [mongoose.Types.ObjectId],
        required: true
    }
}, { timestamps: true })

module.exports = mongoose.model("Group", GroupSchema);