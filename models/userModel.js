const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const UserSchema = new Schema({
    Name: {
        type: String,
        required: true
    },
    PassHash: {
        type: String,
        required: true
    },
    Friends: {
        type: [mongoose.Types.ObjectId],
        required: true
    },
    Groups: {
        type: [mongoose.Types.ObjectId],
        required: true 
    }
}, { timestamps: true });

module.exports = mongoose.model("User", UserSchema);